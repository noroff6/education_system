# Education system / C++

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy](https://www.experis.se/sv/it-tjanster/experis-academy). 
<br />

## Table of Contents
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [References](#references)
- [License](#license)

## Background
The project make use of classes and inheritance in C++. It uses [hps](https://github.com/jl2922/hps) to serialize files. <br/>
Project is still under construction.

<br />
<br />

### Assignment
You are supposed to write a simple console program for an education system. There are a number of students, teachers, and courses.
First create a class hierarchy for the objects found in this system: student, teacher, and course. Use inheritance if applicable. Add appropriate properties to each class.
For the first run, read the students, teachers, and courses information from console.
Store all the information (the created objects) using serialization into a file.
For other runs, you can ask the user if they want the information to be shown on the console.
User can also ask for a specific student, teacher, or course to be shown using an ID. The program should print only that one.
User can ask to assign a student/teacher to a course, or remove it from that.
<br />

## Install
Clone the repo.

**Install CMake:** 
```
$ sudo apt install cmake
```
```
cmake .. //
make
```

**Clone hps**
<br/>
Add this to the root of your project.
```
git clone https://github.com/jl2922/hps.git
```

Include it in your project:
```
#include "../hps/src/hps.h"
```
<br/>

![](images/hps.PNG)
## Usage
```
./main
```
A main menu will appear and you can make choices from that menu.
<br/>
![](images/menu.PNG)


## Contributing
This project does not use any contributors, but feel free to use the code and optimize it in your own unique way.


## License
[MIT](docs/LICENSE.md)


#include "menu.h"

void main_menu()
{
    std::cout << "________________[MAIN MENU]________________" << std::endl;
    std::cout << std::endl;
    std::cout << "1. Add student" << std::endl;
    std::cout << "2. Add teacher" << std::endl;
    std::cout << "3. Add course" << std::endl;
    std::cout << "4. See all students" << std::endl;
    std::cout << "5. See all teachers" << std::endl;
    std::cout << "6. See all courses" << std::endl;
    std::cout << "7. Search student by ID" << std::endl;
    std::cout << "8. Search teacher by ID" << std::endl;
    std::cout << "9. Search course by ID" << std::endl;
    std::cout << "10. Add student to course" << std::endl;
    std::cout << "11. Add teacher to course" << std::endl;
    std::cout << "12. Delete student" << std::endl;
    std::cout << "13. Delete teacher" << std::endl;
    std::cout << "14. Remove student from course" << std::endl;
    std::cout << "15. Remove teacher from course" << std::endl;
    std::cout << "0. Quit program" << std::endl;
    return;
}
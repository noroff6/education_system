#include <iostream>
#include <fstream>
#include<regex>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#pragma once
#ifndef FILE_H
#define FILE_H

class File
{
private:
protected:
public:
    void getAllFiles(std::string file_name);
    std::string getFileByID(std::string ID, std::string file_name);
    void deleteLineInFile(std::string file_name, std::string ID);
    void serializeFile(int ID, std::string name, std::string name2, std::string filename, std::string course_name="Default");
    void deSerializeFile(std::string filePath);
    int getStoredIDs(std::string file_name);
    void saveIDFile(int ID, std::string file_name);
    void splitLine(const std::string &str, std::vector<std::string> &words);

};

#endif
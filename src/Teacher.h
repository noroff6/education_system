#include <iostream>
#include <fstream>
#include "Person.h"
#pragma once
#ifndef TEACHER_H
#define TEACHER_H

class Teacher : public Person
{
    // Constructor inheritance
    using Person::Person;

public:
    int getID();
};
#endif

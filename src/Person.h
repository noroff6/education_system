#include <iostream>
#include <fstream>
#include <cassert>
#include <fstream>

#pragma once
#ifndef PERSON_H
#define PERSON_H

class Person
{
private:
    int id;
    //static int next_id;
    static int savedID(std::string fn);

protected:
    int grade;
    int salary;

public:
    Person(std::string f, std::string l, std::string fn);
    Person();
    std::string first_name;
    std::string last_name;
    std::string file_name;
    int getID();
};

#endif
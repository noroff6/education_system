#include <iostream>
#include <fstream>
#include "File.h"

#pragma once

#ifndef COURSE_H
#define COURSE_H

class Course
{
private:
    int id;
    //static int next_id;
    static int savedID(std::string fn);

public:
    Course(std::string n, std::string d, int p);
    Course();
    std::string course;
    std::string description;
    void add_person_to_course(int person_to_assign, std::string course, std::string role);
    int points;
};

#endif
#include <iostream>
#include "menu.h"
#include <fstream>
#include <cassert>
#include "Person.h"
#include "Student.h"
#include "Teacher.h"
#include "Course.h"
#include "File.h"
#include "../hps/src/hps.h"

int main()
{
    // Variables
    int choice;
    std::string first_name;
    std::string last_name;
    bool user_input = true;
    std::string course_name;
    std::string description;
    int points;
    std::string ID;

    // Files
    const std::string course_file = "courses.txt";
    const std::string teacher_file = "teachers.txt";
    const std::string student_file = "students.txt";

    // Objects
    Teacher teacher;
    Course course;
    Student student;
    File file;

    while (user_input != false)
    {
        main_menu();
        std::cout << std::endl;
        std::cout << "Enter your choice: " << std::endl;
        std::cin >> choice;
        std::cout << std::endl;

        // Switch
        switch (choice)
        {
        // Add new student
        case 1:
        case 2:
        {
            std::cout << "Enter first name: " << std::endl;
            std::cin >> first_name;
            std::cout << "Enter last name: " << std::endl;
            std::cin >> last_name;

            if (choice == 1)
            {
                Student student(first_name, last_name, student_file);
            }
            if (choice == 2)
            {
                Teacher teacher(first_name, last_name, teacher_file);
            }
            break;
        }

        case 3:
        {
            std::cout << "Enter course name: " << std::endl;
            std::cin >> course_name;
            std::cout << "Enter label: " << std::endl;
            std::cin >> description;
            std::cout << "Add total points: " << std::endl;
            std::cin >> points;
            Course new_course(course_name, description, points);
            break;
        }

        // See all students, teachers or courses
        case 4:
        case 5:
        case 6:
        {
            if (choice == 4)
            {
                file.deSerializeFile(student_file);
                std::string temp_file = "temp" + student_file;
                remove(temp_file.c_str());
            }
            if (choice == 5)
            {
                file.deSerializeFile(teacher_file);
                std::string temp_file = "temp" + teacher_file;
                remove(temp_file.c_str());
            }
            if (choice == 6)
            {
                file.deSerializeFile(course_file);
                std::string temp_file = "temp" + course_file;
                remove(temp_file.c_str());
            }
            break;
        }

        // Search by ID: students / teachers / students
        case 7:
        case 8:
        case 9:
        {
            std::cout << "Enter ID to search for: " << std::endl;
            std::cin >> ID;
            std::string result;

            if (choice == 7)
            {
                result = file.getFileByID(ID, student_file);
            }
            if (choice == 8)
            {
                result = file.getFileByID(ID, teacher_file);
            }
            if (choice == 9)
            {
                result = file.getFileByID(ID, course_file);
            }

            break;
        }

        // Add student to course
        case 10:
        {
            std::cout << "Enter ID of student to add: " << std::endl;
            std::cin >> ID;
            std::cout << "Enter course to assign to: " << std::endl;
            std::cin >> course_name;
            std::string person_to_assign = file.getFileByID(ID, student_file);
            int int_ID = atoi(ID.c_str());
            if (person_to_assign == "Error")
            {
                std::cout << "Please try again. " << std::endl;
            }
            else
            {
                //course.add_person_to_course(int_ID, course_name, "Student");
            }
            break;
        }

        // Add teacher to course
        case 11:
        {
            std::cout << "Enter ID of teacher to add: " << std::endl;
            std::cin >> ID;
            std::cout << "Enter course to assign to: " << std::endl;
            std::cin >> course_name;
            std::string person_to_assign = file.getFileByID(ID, teacher_file);
            int int_ID = atoi(ID.c_str());
            if (person_to_assign == "Error")
            {
                std::cout << "Please try again. " << std::endl;
            }
            else
            {
                //course.add_person_to_course(int_ID, course_name, "Teacher");
            }
            break;
        }

        // Delete student
        case 12:
        {
            std::cout << "Enter ID of student: " << std::endl;
            std::cin >> ID;
            file.deleteLineInFile(student_file, ID);
            break;
        }

        // Delete teacher
        case 13:
        {
            std::cout << "Enter ID of teacher: " << std::endl;
            std::cin >> ID;
            file.deleteLineInFile(teacher_file, ID);
            break;
        }

        // Remove student from course
        case 14:
        case 15:
        {
            std::cout << "Enter course name: " << std::endl;
            std::cin >> course_name;
            std::cout << "Enter person ID: " << std::endl;
            std::cin >> ID;
            file.deleteLineInFile(course_name + ".txt", ID);
            break;
        }

        case 16:
        {
            std::string file = "students.txttemp.txt";
            std::string line;
            std::ifstream dataFile;
            std::string filePath = file;

            dataFile.open(filePath);
            if (dataFile.is_open())
            {
                while (getline(dataFile, line))
                {
                    std::string new_file = "testing.txt";
                    std::cout << line << std::endl;
                    
                    // Serialize back
                    std::vector<std::string> data = {line};
                    std::string serialized = hps::to_string(data);
                    std::ofstream dataFileof;
                    dataFileof.open(new_file, std::ios::binary | std::ios::app);
                    dataFileof << serialized << std::endl;
                    dataFileof.close();
                }
                dataFile.close();
            }

            break;
        }

        // Quit program
        case 0:
            exit(0);
            user_input = false;
            break;

        default:
            std::cout << "Invalid entry, try again. " << std::endl;
        }
    }

    return 0;
}
#include "Course.h"
#include <cassert>
#include <iostream>
#include <fstream>
#include "Person.h"

Course::Course(std::string n, std::string d, int p)
{
    const std::string file_name = "courses.txt";
    File file;
    //static int stored_ID = file.getStoredIDs("ID" + fn);
    int saved = savedID("ID" + file_name);

   
    // Add ID to courses
    id = savedID("ID" + file_name) +1;
    course = n;
    description = d;
    points = p;

    // Save ID generated
    file.saveIDFile(id, "ID" + file_name);
    file.serializeFile(id, course, description, file_name);
};

Course::Course(){};

void Course:: add_person_to_course(int person_to_assign, std::string course, std::string role)
{
    //File file;
    std::string filePath_s = course + ".txt";
    //file.serializeFile(person_to_assign, course, role, filePath_s);
    std::ofstream dataFile;
    dataFile.open(filePath_s, std::ios::app);
    dataFile << person_to_assign << " " << "Role: " << role << std::endl;
    dataFile.close();
}

int Course::savedID(std::string fn)
{   File file;
    return file.getStoredIDs(fn);
}
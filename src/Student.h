#include <iostream>
#include <fstream>
#include "Person.h"
#pragma once
#ifndef STUDENT_H
#define STUDENT_H

class Student : public Person
{
    // Constructor inheritance
    using Person::Person;

public:
    int age;
    void setGrade(int g);
    int getGrade();
    void addStudent(std::string f, std::string l);
    int getID();
};
#endif
#include "Person.h"
#include "File.h"

Person::Person(std::string f, std::string l, std::string fn)
{
    File file;
    
    //static int stored_ID = file.getStoredIDs("ID" + fn);
    int saved = savedID("ID" + fn);

    // Add ID to persons
    //id = next_id++;
    id = savedID("ID" + fn) +1;
    first_name = f;
    last_name = l;
    file_name = fn;

    // Save ID generated
    file.saveIDFile(id, "ID" + fn);

    // Serialize file for persons
    file.serializeFile(id, f, l, fn);
}

Person::Person(){}

// Return ID
int Person::getID() { return id; };
//int Person::next_id = 1;

int Person::savedID(std::string fn)
{   File file;
    return file.getStoredIDs(fn);
}


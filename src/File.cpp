#include "File.h"
#include "../hps/src/hps.h"

void File::saveIDFile(int ID, std::string file_name)
{
    std::ofstream dataFile;
    std::string filePath_s = file_name;
    dataFile.open(filePath_s, std::ios::app);
    dataFile << ID << std::endl;
    dataFile.close();
}

// Get stored IDs
int File::getStoredIDs(std::string file_name)
{

    std::string line;
    std::ifstream dataFile;
    std::string filePath = file_name;
    int ID = 1;

    dataFile.open(filePath);
    if (dataFile.is_open())
    {
        while (getline(dataFile, line))
        {
            ID = std::atoi(line.c_str());
        }
        dataFile.close();
        return ID;
    }
    else
    {
        std::cout << "File read error!" << std::endl;
        return 1;
    }

    return 1;
}

void File::serializeFile(int ID, std::string name, std::string name2, std::string filename, std::string course_name)
{
    std::pair<int, std::pair<std::string, std::string>> pair1 = {ID, {name, name2}};
    std::vector<std::pair<int, std::pair<std::string, std::string>>> data = {pair1};

    std::string serialized = hps::to_string(data);

    std::ofstream dataFile;
    dataFile.open(filename, std::ios::binary | std::ios::app);
    dataFile << serialized << std::endl;
    dataFile.close();
}

void File::deSerializeFile(std::string filePath)
{
    std::ofstream file;
    std::string filePath_s = "temp" + filePath;
    file.open(filePath_s, std::ios::app);

    std::vector<std::pair<int, std::pair<std::string, std::string>>> data;
    std::string line;
    std::ifstream dataFile;

    dataFile.open(filePath, std::ios::binary);
    if (dataFile.is_open())
    {
        std::string dataRead;
        while (getline(dataFile, line))
        {
            data = hps::from_string<std::vector<std::pair<int, std::pair<std::string, std::string>>>>(line);

            for (std::pair<int, std::pair<std::string, std::string>> entry : data)
            {
                std::cout << entry.first << " " << entry.second.first << " " << entry.second.second << std::endl;

                file << entry.first << " " << entry.second.first << " " << entry.second.second << std::endl;
            }
        }

        file.close();
        dataFile.close();
    }
    else
    {
        std::cout << "File read error!" << std::endl;
    }
}

void File::getAllFiles(std::string file_name)
{
    std::string line;

    std::ifstream dataFile;

    std::string filePath = file_name;

    dataFile.open(filePath);
    if (dataFile.is_open())
    {
        while (getline(dataFile, line))
        {
            std::cout << line << std::endl;
        }
        dataFile.close();
    }
    else
    {
        std::cout << "File read error!" << std::endl;
    }
}

std::string File::getFileByID(std::string ID, std::string file_name)
{
    std::string line;
    std::ifstream dataFile;
    std::string filePath = file_name + "temp.txt";
    std::string return_value;

    dataFile.open(filePath);
    if (dataFile.is_open())
    {
        while (getline(dataFile, line))
        {
            if (line.find(ID) != std::string::npos)
            {
                std::cout << line << std::endl;
                return_value = line;
                break;
            }
            else
            {
                return_value = "Error";
            }
        }
        dataFile.close();
    }
    else
    {
        std::cout << "File read error!" << std::endl;
    }

    return return_value;
}

void File::splitLine(const std::string &str, std::vector<std::string> &words)
{
    std::istringstream iss(str);
    std::copy(std::istream_iterator<std::string>(iss),
              std::istream_iterator<std::string>(),
              back_inserter(words));

    for (int i = 0, sz = words.size(); i < sz; i++)
    {
        std::string word = words.at(i);
    }
}

void File::deleteLineInFile(std::string file_name, std::string ID)
{

    // Deserialize file name
    deSerializeFile(file_name);
    std::string temp_file = "temp" + file_name;

    std::string line;

    // Open deserialized file
    std::ifstream dataFile;
    std::string filePath = temp_file;
    dataFile.open(filePath);

    // For serialization
    std::ofstream dataFileof;

    std::string temp_test = "testing_test.txt";

    while (getline(dataFile, line))
    {
        if (line.find(ID) != std::string::npos)
        {
            line = std::regex_replace(line, std::regex(line), "");
        }

        std::cout << "LINE: " << line << std::endl;
        std::vector<std::string> words;

        splitLine(line, words);

        for (int i = 0, sz = words.size(); i < sz; i++)
        {
            // std::cout << "Words: " << words.at(2) << std::endl;
            int converted_id = atoi(words.at(0).c_str());

            
            // Serialize back
            serializeFile(converted_id, words.at(1), words.at(2), temp_test);
            // Remove and rename file
            break;
        }
    }
    

    dataFileof.close();
    dataFile.close();

    // Remove and rename file
    remove(file_name.c_str());
    rename(temp_test.c_str(), file_name.c_str());
}
